import sublime, sublime_plugin
import os, re

class CppCreateCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        # make sure the current file is a cpp header file
        if self.view.file_name().rfind(".h") == -1:
            print("Error: Current file is not a C++ header file.")
            return

        # open the current file
        f = open(self.view.file_name(), "r")

        # go through the header file and get all the prototypes from the class
        inclass = False
        classname = ""
        prototypes = []
        for line in f:
            line = line.strip()
            if line.find("class") != -1:
                inclass = True
                classname = line.split(" ")[1].strip()
            if line.find('(') != -1 and line.find(')') != -1 and line.find("inline") != 0 and inclass:
                if line.find("//") != -1:
                    prototypes.append(line.split("//")[0].strip()[:-1])
                else:
                    prototypes.append(line[:-1])

        # open the cpp file to generate and save it
        if os.path.exists(self.view.file_name()[:-1]+"cpp"):
            o = open(self.view.file_name()[:-1]+"cpp.gen", "w")
        else:
            o = open(self.view.file_name()[:-1]+"cpp", "w")

        # go through every prototype and write the definition for it
        for func in prototypes:
            if func.find("virtual ") != -1:
                func = func[8:]

            if func.find(classname + "(") != -1:
                o.write(classname + "::" + func)
            else:
                func_split = re.split("\(|\)", func)
                func_temp=func_split[0].split(" ")

                func_type = " ".join(func_temp[:len(func_temp) - 1])
                func_name = func_temp[len(func_temp) - 1] + "(" + func_split[1] + ")"
                func_const = func_split[2]
                o.write(func_type + " " + classname + "::" + func_name + " " + func_const)

            o.write("\n{\t\n\n}\n\n\n")

        # close the files
        o.close()
        f.close()

