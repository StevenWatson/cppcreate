# CppCreate
## What is CppCreate?
CppCreate is a quick Sublime Text 3 plugin that takes a C++ header files containing a class definition and creates a C++ source file with definitions created automatically, removing the tedious nature of creating the initial function definitions

## Features
- Generate a class source file with function definitions from a class header file
- Handles virual functions, const functions and functions that return const types

## Future
- Ignore variable default values in functions
- Add default key binding
- Added some control over what is created

